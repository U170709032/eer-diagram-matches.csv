drop database if exists tennis_db;
create database if not exists tennis_db;
use tennis_db;


create table Players(
	player_id int primary key,
    player_name varchar(20),
    player_age float,
    player_rank int,
    player_ioc varchar(20),
    player_hand char(1),
    player_entry varchar(5),
    player_rank_points int,
    player_seed int

);

insert into Players(player_id,player_name,player_age,player_rank,player_ioc,player_hand,player_entry,player_rank_points,player_seed) values(200001,"Martina Hingis", 19.2963723477,1,"SUI","R",NULL,6003,1);
insert into Players(player_id,player_name,player_age,player_rank,player_ioc,player_hand,player_entry,player_rank_points,player_seed) values(200002,"Mirjana Lucic", 17.8590006845,49,"CRO","R",NULL,640,NULL);


create table Tourneys(
	id int primary key auto_increment,
    Tourney_id varchar (30),
    Tourney_name varchar (40),
    Tourney_date int,
    draw_size int,
    surface varchar(10),
    Tourney_year int,
    Tourney_level varchar(5)
    
);


insert into Tourneys(id, Tourney_id, Tourney_name, Tourney_date,draw_size,surface,Tourney_year,Tourney_level) values ( 1, "2000-W-SL-AUS-01A-2000", "Australian Open", 20000117,128,"Hard",2000,"G");


create table Matches(
	match_id int,
    Tourney_id int,
    winner_id int ,
    loser_id int ,
    round varchar(5),
    best_of int,
    minutes int,
    score varchar(10),
    foreign key (winner_id) references Players(player_id),
    foreign key (loser_id) references Players(player_id),
    foreign key (Tourney_id) references Tourneys(id),
    primary key (match_id, Tourney_id)
);

insert into Matches(match_id, Tourney_id, winner_id, loser_id, round,best_of,minutes,score) values(1, 1, 200001, 200002, "R128",3,NULL,"6-1 6-2");
