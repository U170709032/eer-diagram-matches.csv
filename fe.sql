-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`Players`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Players` (
  `player_id` INT NOT NULL,
  `player_name` VARCHAR(45) NOT NULL,
  `player_age` FLOAT NULL,
  `player_hand` CHAR(1) NULL,
  `player_rank` INT NULL,
  `player_rank_points` VARCHAR(45) NULL,
  `player_ioc` VARCHAR(45) NULL,
  `player_entry` VARCHAR(5) NULL,
  `player_seed` INT NULL,
  PRIMARY KEY (`player_id`))
ENGINE = InnoDB;

insert into Players(player_id,player_name,player_age,player_rank,player_ioc,player_hand,player_entry,player_rank_points,player_seed) values(200001,"Martina Hingis", 19.2963723477,1,"SUI","R",NULL,6003,1);
insert into Players(player_id,player_name,player_age,player_rank,player_ioc,player_hand,player_entry,player_rank_points,player_seed) values(200002,"Mirjana Lucic", 17.8590006845,49,"CRO","R",NULL,640,NULL);



-- -----------------------------------------------------
-- Table `mydb`.`Tourneys`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Tourneys` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `Tourney_id` VARCHAR(45) NOT NULL,
  `Tourney_date` INT NULL,
  `Tourney_level` CHAR(1) NULL,
  `tourney_name` VARCHAR(45) NOT NULL,
  `draw_size` INT NULL,
  `surface` VARCHAR(45) NULL,
  `Tourney_year` INT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `Tourney_id_UNIQUE` (`Tourney_id` ASC))
ENGINE = InnoDB;

insert into Tourneys(id, Tourney_id, Tourney_name, Tourney_date,draw_size,surface,Tourney_year,Tourney_level) values ( 1, "2000-W-SL-AUS-01A-2000", "Australian Open", 20000117,128,"Hard",2000,"G");
-- -----------------------------------------------------
-- Table `mydb`.`Matches`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Matches` (
  `match_id` INT NOT NULL AUTO_INCREMENT,
  `Tourney_id` INT NOT NULL,
  `winner_id` INT NOT NULL,
  `loser_id` INT NOT NULL,
  `best_of` INT NULL,
  `round` VARCHAR(45) NULL,
  `minutes` INT NULL,
  `score` VARCHAR(20) NULL,
  INDEX `fk_matches_Player_idx` (`winner_id` ASC),
  INDEX `fk_matches_Player1_idx` (`loser_id` ASC),
  PRIMARY KEY (`match_id`, `Tourney_id`),
  CONSTRAINT `fk_matches_Player`
    FOREIGN KEY (`winner_id`)
    REFERENCES `mydb`.`Players` (`player_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_matches_Player1`
    FOREIGN KEY (`loser_id`)
    REFERENCES `mydb`.`Players` (`player_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_matches_Tourney1`
    FOREIGN KEY (`Tourney_id`)
    REFERENCES `mydb`.`Tourneys` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
insert into Matches(match_id, Tourney_id, winner_id, loser_id, round,best_of,minutes,score) values(1, 1, 200001, 200002, "R128",3,NULL,"6-1 6-2");

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
